![Icon_CC.png](https://bitbucket.org/repo/kbKzbq/images/3946447463-Icon_CC.png)

### What is ClonoCalc? ###

ClonoCalc provides a GUI for the sub-tasks of processing raw NGS data by wrapping available implementations of various algorithms. ClonoCalc runs on Linux and Mac OS X. 
Windows systems are not supported directly. 
A UNIX shell script is used to call algorithm implementations, thus providing advanced users easy means of replacing them with alternatives.
ClonoCalc's Java front-end handles only the user input, while the main processing part is passed to the shell script. The script itself executes all tools for the individual analysis steps and aggregates their output. ClonoCalc ships with interfaces to a series of current well-established tools and comprises the following features.

* Input and demultiplexing of samples: If multiple samples shall be analyzed ClonoCalc first splits the raw data (FASTQ format) and classifies the individual reads according to their barcode. The user selects input and output files for the analysis and enters the barcodes to identify specific samples. A readable name can be chosen for every sample and the user is supported by an auto completion and error correction system.

* Clonotype determination: Raw NGS data is processed by the tool MiXCR  that performs paired-end read merging if using the Illumina system.  Despite this option MiXCR  is choosen for implementation processing raw NGS data as one of the most efficient and accurate tools to extract human or animal source BCR and TCR clonotypes providing corrections of erroneous sequences introduced by NGS [MiXCR](https://github.com/milaboratory/mixcr).  The call parameters, i.e.\ processing options, for MiXCR are read from a configuration file.

* Automatic documentation: A report of the data analysis process will be saved automatically. 
This allows the user to review, track and save specific options. 
The call parameters (options) can be modified if desired and will be stored for later reproduction of the analysis. A log file is stored that contains all information on the performed run, including the configuration parameters and the output of MiXCR. 

* Flexibility: Other back-ends replacing MiXCR may be used by adjusting the call script. This modular script-based approach allows for modification and extension without the need for recompilation.

### What do you need? ###
* R, Java and **Unix-Shell compatible OS** like Linux or Mac OS X
* Java Version 1.8 or later (needs ~ 300 MB free disk space)
* R Version 3.2.2 or later (needs ~ 500 MB free disk space)
* R-package *data.table*

### How do I get set up? ###

First download the file [*ClonoCalc.zip*](https://bitbucket.org/uniluebeckclonocalcplot/clonocalc/raw/master/ClonoCalc.zip).
Extract the content of *ClonoCalc.zip* to a local folder (called installation folder). ClonoCalc use this installation folder as a temporary folder, i.e there must exists some GB of free space temporary files (of course you need also the permission to write).

There is done some post-processing by [R](http://mirrors.softliste.de/cran/).

**Linux:**
Linux users should use the system package manager (e.g. Ubuntu Software Center) to install R. Alternatively, open a console and type *sudo apt-get install r-base* to install R.

** Mac OS X:**
R can be downloaded [here](http://mirrors.softliste.de/cran/bin/macosx/). 

Subsequently add the bin folder of R to the system PATH-variable. To check if you have done this successfully, simply open a console, type *Rscript* and press enter. If this works without any errors, everything seems fine.

To do some post-processing ClonoCalc uses the R-package
[data.table](https://cran.r-project.org/web/packages/data.table/index.html),  
To install an R-package simply start a console, type in *R* to start R and type in *install.packages("data.table", dependencies=T)* to install the package.

Finally there have to exists the following files in your installation folder:

```
<folder>installation folder
    BarcodeSplitter_ReverseReadSearch.jar
    ClonoCalc.jar
    config.txt
    illumina_barcodes.txt
    meins.sh
    mixcr.jar
    <folder>R
        autoR.sh
        template_non_tcr.R
        template_tcr.R
```
Now you can start ClonoCalc from the terminal *java -jar ClonoCalc.jar* or with a double-click in the file browser if you linked the java runtime with jar-files.
To test ClonoCalc you can download the provided files [*sample_R1_50k.fastq.gz*](https://bitbucket.org/uniluebeckclonocalcplot/clonocalc/raw/master/testData/sample_R1_50k.fastq.gz) and [*sample_R2_50k.fastq.gz*](https://bitbucket.org/uniluebeckclonocalcplot/clonocalc/raw/master/testData/sample_R2_50k.fastq.gz). When you first start ClonoCalc, there already filled some basic input fields related to the sample files.
For more information about how to get started, use the [ClonoCalc Tutorial](https://bitbucket.org/uniluebeckclonocalcplot/clonocalc/raw/master/ClonoCalc_Tutorial.pdf).

### Who do I talk to? ###
Feel free to ask questions to faehnrich@anat.uni-luebeck.de