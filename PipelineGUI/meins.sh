#!/bin/bash
#PATH="/home/peter/circos-0.68-pre1/bin:/home/peter/DNA/bin:$PATH"
#PATH="/home/peter/Desktop/eclipse/jre/bin:$PATH"
#PATH="/home/peter/git/pipeline/PipelineGUI:$PATH"

#get input parameters
OWNDEMULTIPLEXER=0
SILENT=0
AUTOBARCODEFILE=0
KIEL=1
ERRORCHECK=1
USEPEAR=0
USEMITCR=0
USEMIXCR=0
USEMIXCRASSEMBLING=0
FLASH=0
LINUX=0
MAXHAMMINGDIST=0
BARCODESTARTPOS=3
#do that to supress unit variables
i=0
length="$#"
while [ "$length" -ne 0 ] && [ -n "$1" ]
  do
	#echo $1
	#double square bracket to supress errors
	[[ $1 == *"-cd "* ]] && echo cd $(echo $1|cut -c 5-) && cd $(echo $1|cut -c 5-)
	[[ $1 = "-silent" ]] && SILENT=1 && echo "no interrupts"
	[[ $1 = "-owndemultiplexer" ]] && OWNDEMULTIPLEXER=1 && echo "use own demultiplexer"
	[[ $1 = "-pear" ]] && USEPEAR=1 && echo "use pear as assembler"
	[[ $1 = "-flash" ]] && FLASH=1 && echo "use flash as assembler"
	[[ $1 = "-mitcr" ]] && USEMITCR=1 && echo "use mitcr"
	[[ $1 = "-mixcr" ]] && USEMIXCR=1 && echo "use mixcr"
	[[ $1 = "-mixcrAssembling" ]] && USEMIXCRASSEMBLING=1 && echo "use mixcr assembling"
	[[ $1 = "-linux" ]] && LINUX=1 && echo "linux os"
	[[ $1 = *"-maxHammingDist"* ]] && [[ $1 =~ -[A-Za-z]*([0-9]*) ]] && MAXHAMMINGDIST=${BASH_REMATCH[1]} && echo "max hamming distance = ${MAXHAMMINGDIST}"
	[[ $1 = *"-barcodeStartPos"* ]] && [[ $1 =~ -[A-Za-z]*([0-9]*) ]] && BARCODESTARTPOS=${BASH_REMATCH[1]} && echo "barcode start pos = ${BARCODESTARTPOS}"
	[[ $1 = "-noerrorcheck" ]] && ERRORCHECK=0 && echo "no error checks"
	[[ $1 = "-auto" ]] && AUTOBARCODEFILE=1 && echo "automatic barcode generation"
	[[ $1 = "-wKiel" ]] && KIEL=0 && echo "automatic barcode generation"
	((i++))
	if ((i < length))
	then
		shift
	else
		break
	fi
done
readonly SILENT
readonly AUTOBARCODEFILE
readonly KIEL
readonly ERRORCHECK
readonly USEPEAR
readonly USEMITCR
readonly USEMIXCR
readonly USEMIXCRASSEMBLING
readonly FLASH
readonly OWNDEMULTIPLEXER
readonly LINUX
readonly MAXHAMMINGDIST
readonly BARCODESTARTPOS

pwd=$(pwd)
PATH="${pwd}:$PATH"
echo $PATH

#look for important binarys
[ -f illumina_barcodes.txt ] || (echo "illumina_barcodes.txt does not exist!" && exit)

if [ $OWNDEMULTIPLEXER == 0 ]
then
	[ -f BarcodeSplitter_ReverseReadSearch.jar ] || (echo "BarcodeSplitter_ReverseReadSearch.jar does not exist!" && exit)
fi

if [ $FLASH == 0 ] && [ $USEPEAR == 1 ]
then
	[ -f pear ] || (echo "pear does not exist!" && exit)
	[ -x pear ] || (echo "pear is not executable. Try to add owner excution rights (chmod u+x pear)." && chmod u+x pear \
		&& (([ -x pear ] && echo "pear is now excutable.") || (echo "failed to set execution rights for pear." && exit)))
else
	if [ $FLASH == 1 ]
	then
		if [ $LINUX == 1 ]
		then
			[ -f flash_linux_x64 ] || (echo "flash does not exist!" && exit)
		else
			[ -f flash_macOSX ] || (echo "flash does not exist!" && exit)
		fi
	fi
fi

if [ $USEMITCR == 1 ]
then
	[ -f mitcr.jar ] || (echo "mitcr.jar does not exist!" && exit)
fi

#save the current time for logs
starttime=$(date +"%d-%m-%y_%H_%M_%S")
readonly starttime
rm "lastTime.txt" 2> /dev/null
echo $starttime >> "lastTime.txt"

#----------------READ IN CONFIG FILE------------------------------------
#config variables
#barcodefile 		f.e. barcodes.txt
#unpackedfastq		f.e. SRBC-19_S1_L001_R1_001.fastq
#packedfastqrev		f.e. SRBC-19_S1_L001_R2_001.fastq.gz
#mincount		f.e. 10000

#saves the names for the files which are created in the different processes
filenamesfile="filenames.txt"
#saves the barcodes which are used to create a file for the barcode splitter
barcode="barcodes.txt"
config="config.txt"
cryoNrfile="cyroNr.txt"
rm $cryoNrfile
readonly config
readonly cryoNrfile

#define arrays for evalution
cryoNrarr=()
IDarr=()

#define substitutions for escape chars
subsDot="dot"
subsBackslash="bslash"
subsUnderscore='\\\_'
readonly subsDot
readonly subsBackslash
readonly subsUnderscore

unknownkeyword="no"
if [ -f $config ]
then
	#delete	filenamesfile and barcode before read in config
	if [ -f $filenamesfile ]
	then
		#read -p "${filenamesfile} will now be deleted! Press any key to continue." ok
		rm $filenamesfile
	fi

	if [ -f $barcode ]
	then
		#read -p "${barcode} will now be deleted! Press any key to continue." ok
		rm $barcode
	fi
	echo -e "-------------------------------CONFIG FILE VARIABLES BEGIN-------------------------------"
	while read line; do
		if [[ $line == *"#"* ]] || [[ -z "$line" ]]
		then
			continue
		fi
		#split line by "=" in name and value
		#tr -d '\040\011\012\015' will remove spaces, tabs, carriage returns and newlines
		name=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f1) 
		if [[ $line != *"\""* ]]
		then	#example: count = 10000
			value=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f2)
		else	#example: string = "hello world"
			#get the substring between the quotes
 			value=$(echo $line | cut -d'"' -f2)
		fi
		if [[ $name == "ForwardReadFile" ]]
		then
			#get value from the parameter => index+1
			echo "ForwardReadFile = $value";
			unpackedfastq=$value;
		elif [[ $name == "ReverseReadFile" ]]
		then
			echo "ReverseReadFile = $value";
			packedfastqrev=$value;
		elif [[ $name == "minCount" ]]
		then
			echo "minCount = $value";
			mincount=$value;
		elif [[ $name == "PARAbarcodesplitter" ]]
		then
			echo "PARAbarcodesplitter = $value";
			PARAbarcodesplitter=$value;	
		elif [[ $name == "PARApear" ]]
		then
			echo "PARApear = $value";
			PARApear=$value;
		elif [[ $name == "PARAmitcr" ]]
		then
			echo "PARAmitcr = $value";
			PARAmitcr=$value;
		elif [[ $name == "PARAmitcrTCRPACKAGE" ]]
		then
			echo "PARAmitcrTCRPACKAGE = $value";
			PARAmitcrTCRPACKAGE=$value;
		elif [[ $name == "PARAmixcrAlign" ]]
		then
			echo "PARAmixcrAlign = $value";
			PARAmixcrAlign=$value;
		elif [[ $name == "PARAmixcrAssemble" ]]
		then
			echo "PARAmixcrAssemble = $value";
			PARAmixcrAssemble=$value;
		elif [[ $name == "PARAmixcrExportClones" ]]
		then
			echo "PARAmixcrExportClones = $value";
			PARAmixcrExportClones=$value;
		elif [[ $name == "BarcodeFile" ]]
		then
			echo "BarcodeFile = $value";
			barcode=$value;
		elif [[ $name == "OutputPath" ]]
		then
			echo "OutputPath = $value";
			outputpath=$value;
		elif [[ $line == *"&"* ]]
		then
			first=$(echo $line |  tr -d '\040\011\012\015' | cut -d\& -f1 \
			| sed -E "s/[.]+/${subsDot}/g" | sed -E "s/[\\]+/${subsBackslash}/g") #cyro-nr

			second=$(echo $line |  tr -d '\040\011\012\015' | cut -d\& -f2 \
			| sed -E "s/[.]+/${subsDot}/g" | sed -E "s/[\\]+/${subsBackslash}/g") #ID

			third=$(echo $line |  tr -d '\040\011\012\015' | cut -d\& -f3  \
			| sed -E "s/[.]+/${subsDot}/g" | sed -E "s/[\\]+/${subsBackslash}/g") #barcode
			
			#build file with cyro-nr for later R-analyse
			echo "${first}" >> $cryoNrfile
			cryoNrarr+=( "${first}" )
			IDarr+=( "${second}" )
			#build up empty barcodes
			#it's seems useless to write the barcodes into a file and read the file
			#but this compatible with AUTOBARCODEFILE
			echo "${third}" >> $barcode
		else
			echo -e "UNKNOWN KEYWORD '$name' IN $config FOUND!"
			unknownkeyword="yes"
		fi
	done < $config
	
	
	#valid check
	#if [[ $unpackedfastq != *"R1"* ]]
	#then
	#	read -p "is $unpackedfastq the FORWARD read? " ok
	#fi
	#if [[ $unpackedfastq == *"gz"* ]]
	#then
	#	read -p "is $unpackedfastq UNPACKED(non gz)? " ok
	#fi
	#if [[ $packedfastqrev != *"R2"* ]]
	#then
	#	read -p "is $packedfastqrev the RESERVE read? " ok
	#fi
	#if [[ $unknownkeyword == *"yes"* ]]
	#then
	#	 read -p "unknown keyword(s) found! Continue? " ok
	#fi
	
	
	echo -e "-------------------------------CONFIG FILE VARIABLES END---------------------------------"
else
	#if no config file is found use barcodes as file names
	echo "no $config file found";
	filenamesfilearr=$(cat $barcode) 
fi
readonly unpackedfastq
readonly packedfastqrev
readonly mincount
readonly PARAbarcodesplitter
readonly PARApear
readonly PARAmitcr
readonly PARAmitcrTCRPACKAGE
readonly PARAmixcrAlign
readonly PARAmixcrAssemble
readonly PARAmixcrExportClones
readonly barcode
readonly cryoNrarr
readonly IDarr
#---------------READ IN CONFIG FILE END------------------------------------

#wait
if [ $SILENT != 1 ]
	then
		read -p "generate barcodefile? " ok
fi

barcodefile="BCfiletmp.txt"
readonly barcodefile
rm $barcodefile 2> /dev/null

if [ $AUTOBARCODEFILE == 0 ]
	then
		if [ ! -f $barcode ]
			then
				echo "barcode file $barcode not found"
				exit
		fi
fi

#---------------CREATE BARCODE AND NAME FILE------------------------------------
if [ $AUTOBARCODEFILE == 1 ]
   then	
	#paste - - - -  => combine 4 lines to 1 line with TAB as a delimiter
	#cut -f2 => get the second "column"(second delimiter) and delete the rest
	#cut -b4-9 => get char 4-9 from the string and delete the rest
	#we have to sort the input before we can get a count for each seq
	#sort ascending by counts -n => numeric
	index=$(cat $unpackedfastq|paste - - - - |cut -f2|cut -b4-9|sort|uniq -c|sort -n)
	if [ -f $barcode ]
	then
		read -p "${barcode} will now be deleted? " ok
		rm $barcode 2> /dev/null
	fi
	#filter out seq with a count greater equal mincount and create a barcodefile with this seq's

	i=0
	boundary=0
	for var in $index; do
		#if seq count greater than mincount then use the seq
		if (( $i % 2 == 0 )) && ((var >= mincount))
			then
				((boundary=1))
		fi
		#check if current seq count is greater than mincount	
		if (( $i % 2 == 1 )) && ((boundary > 0))
			then
				echo "$var" >> $barcode
		fi
		((i++))
	done
	#if no config file is found use barcodes as file names
	filenamesfilearr=$(cat $barcode) 
fi

#read in barcodes
#barcodearr=$(cat $barcode) 

while IFS=\= read var; do
    barcodearr+=($var)
done < $barcode
#readarray -t barcodearr < $barcode

if [ -f $config ]
then
	#find the i's to the barcodes i.e. "i1 GTGTCA" and save them in a ArrayList
	#in addition this routine creates filenamesfile
	iNRarr=()
	illuminabarcodesfile="illumina_barcodes.txt"
	readonly illuminabarcodesfile

	for i in "${!barcodearr[@]}";do
		#this garuntes that each barcode has an i-Nr
		iNr="error"
		while read line; do
			#if line contains # than skip
			if [[ $line == *"#"* ]]
			then
				continue
			fi
			#split line by "=" in name and value
			#tr -d '\040\011\012\015' will remove spaces, tabs, carriage returns and newlines
			possibleI=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f1)
			code=$(echo $line |  tr -d '\040\011\012\015' | cut -d\= -f2)
			if [[ $code == ${barcodearr[i]} ]]
			then
				#get value from the parameter => index+1
				iNr=$possibleI
			fi
		done < $illuminabarcodesfile
		if [[ $iNr == *"error"* ]]
		then
			echo "Don't find a iNr for code ${var}"
			read -p "continue? " ok
		fi
		iNRarr+=( "${iNr}" )
		#build up empty filenamesfile
		echo "${cryoNrarr[i]}_${IDarr[i]}_${barcodearr[i]}" >> $filenamesfile
	done
fi

#read in filenames -t exclude newline
#filenamesfilearr=$(cat $filenamesfile) 
while IFS=\= read var; do
    filenamesfilearr+=($var)
done < $filenamesfile
#readarray -t filenamesfilearr < $filenamesfile

#write in new barcode file i.e. "BC1 ACCTGA"
for i in "${!filenamesfilearr[@]}";do
	echo "${filenamesfilearr[i]} ${barcodearr[i]}" >> $barcodefile
	#echo "$var $var" >> $barcodefile
done

#initalization completed, now it's time to save config arrs
readonly barcodearr
readonly filenamesfilearr
readonly iNRarr

#show created barcodefile
echo -e "-------------------------------BARCODEFILE BEGIN-------------------------------"
	cat $barcodefile
echo -e "-------------------------------BARCODEFILE END---------------------------------"
#---------------CREATE BARCODE AND NAME FILE END----------------------------------


#wait
if [ $SILENT != 1 ]
	then
		read -p "continue? " ok
fi

#-----------------BEGIN DATA PROCESSING-------------------------------------
#define binary paths
PATHbarcodesplitter="perl fastx_barcode_splitter.pl"
PATHKiel="Kiel"
PATHOwnBarcodeSplitterReverseReadSearch="BarcodeSplitter_ReverseReadSearch.jar"
PATHpear="pear"
PATHmitcr="mitcr.jar"
PATHmitcrViewer="mitcr.viewer.jar"
PATHmixcr="mixcr.jar"
PATHflash="flash_macOSX"
if [ $LINUX == 1 ]
then
	PATHflash="flash_linux_x64"
fi
if [ -f flash ]
then
	PATHflash="flash"
fi
#PARAmitcrTCRPACKAGE="-species mm -gene TRB -pset flex -cysphe 0 -level 2"
#if [ -z ${PARAmitcr+x} ]
#then
#	PARAmitcr="-pset -flex -species mm -gene TRB -cysphe 0"
#fi

#DFEINE OUTPUT PATH
#assume that the script run in Lauf_xx/bin
#each run create new folder in Lauf_xx/01-01-15_10_10_10
#this folder named by starttime of the script contains all output data and logs
outputpath="${outputpath}/${starttime}"
#outputpatch is now defined in the config file
mkdir ${outputpath} 2> /dev/null

outputfoldertables="tables"
mkdir ${outputpath}/${outputfoldertables} 2> /dev/null

#DEFINE LOG PATH
#create log dir
logpath="${outputpath}/log"
mkdir ${logpath}
#define log files
kiellogfile="${logpath}/kiellog.txt"
assemblerlogfilepath="${logpath}/assembler"
mkdir $assemblerlogfilepath
barcodesplittlogfile="${logpath}/bcsplitterlog.txt"

texlogfile="${logpath}/overview.txt"
texlogfilehighprecision="${logpath}/overview_high_precision.txt"

OwnBarcodeSplitterReverseReadSearchParameter=""
if [ $FLASH == 1 ]
then
	OwnBarcodeSplitterReverseReadSearchParameter="-cut 25"
fi
OwnBarcodeSplitterReverseReadSearchParameter=`echo $OwnBarcodeSplitterReverseReadSearchParameter -maxHammingDist ${MAXHAMMINGDIST} -barcodeStartPos ${BARCODESTARTPOS}`
APE=ape
BEAR=bear
APEBEAR=`echo $APE $BEAR`

#mitcrlogfile="${logpath}/mitcrlog.txt"


#create command for fastx barcode splitter
if [ $OWNDEMULTIPLEXER != 1 ]
then
	echo -e "------------------------OUTPUT BARCODE SPLITTER--------------------------------"
	#check if forward read is compressed or not
	if [[ ${unpackedfastq: -3} != ".gz" ]]
	then
		echo "cat $unpackedfastq | $PATHbarcodesplitter --bcfile $barcodefile $PARAbarcodesplitter >> $barcodesplittlogfile"
		cat $unpackedfastq | ($PATHbarcodesplitter --bcfile $barcodefile $PARAbarcodesplitter >> $barcodesplittlogfile) 2> error.txt
	else
		echo "gunzip -c $unpackedfastq | $PATHbarcodesplitter --bcfile $barcodefile $PARAbarcodesplitter >> $barcodesplittlogfile"
		gunzip -c $unpackedfastq | ($PATHbarcodesplitter --bcfile $barcodefile $PARAbarcodesplitter >> $barcodesplittlogfile) 2> error.txt
	fi

	bssplitterlogTmp=$(cat error.txt)
	if [[ ${bssplitterlogTmp} == *"Error"* ]]
	then
		cat error.txt
		echo "barcode splitter error detected."
		read -p "continue? " ok
	else
		cat $barcodesplittlogfile
		rm error.txt
	fi
	echo -e "------------------------OUTPUT BARCODE SPLITTER END----------------------------"

	#create directory for every sequence and move the files into
	for var in "${filenamesfilearr[@]}"; do
		mkdir ${outputpath}/$var;
		mv $var.fastq ${outputpath}/$var;
	done

	#remove unmatched file
	rm unmatched.fastq

	#wait...
	if [ $SILENT != 1 ]
		then
			read -p "continue?" ok
	fi

	#print first 8 lines of each file (two reads)
	#for var in "${filenamesfilearr[@]}"; do
	#	echo -e "------------------------$var BEGIN------------------------------------"
	#	cat ${outputpath}/$var/$var.fastq | head -8
	#	echo -e "------------------------$var.fastq END--------------------------------"
	#done


	if [ $KIEL == 1 ]
	then
		#wait...
		if [ $SILENT != 1 ]
			then
				read -p "continue with Kiel?" ok
		fi

		echo -e "------------------------KIEL BEGIN--------------------------------"

		#perl Kiel for each mouse (each .fastq from the barcode splitter)
		for var in "${filenamesfilearr[@]}"; do
			echo "perl $PATHKiel -f ${outputpath}/$var/$var.fastq -f2 $packedfastqrev > ${outputpath}/$var/${var}_rev.fastq"
			echo "perl $PATHKiel -f ${outputpath}/$var/$var.fastq -f2 $packedfastqrev > ${outputpath}/$var/${var}_rev.fastq" \
			>> $kiellogfile

			perl $PATHKiel -f ${outputpath}/$var/$var.fastq -f2 $packedfastqrev > ${outputpath}/$var/${var}_rev.fastq
	
			if [ $ERRORCHECK == 1 ]
			then
				#check if reserve string differs only in one pos for first 5 'headlines'
				for i in {1..5}
				do
					#cut the every 4th line out of the file
					r1=$(sed -n "$((4*i+1))p" ${outputpath}/$var/$var.fastq)
					r2=$(sed -n "$((4*i+1))p" ${outputpath}/$var/${var}_rev.fastq)
					#check if lines differs in only one pos
					diffcount=0
					for (( i=0; i<${#r1}; i++ )); do
						c1=${r1:$i:1}
						c2=${r2:$i:1}
					  if [ "$c1" != "$c2" ]
					  then
						if [ "$c1" == "1" ] && [ "$c2" == "2" ]
						then
							((diffcount++))
						fi
					  fi
					done
					#if string differs more or less, let the user decide what to do
					if (( $diffcount != 1 ))
					then
						echo "an error was found in Kiel ${var}!"
						echo $r1
						echo $r2
						read -p "continue? " ok
					#else
						#echo "${var}_rev.fastq seems ok"
					fi
				done
			fi
		done

		echo -e "------------------------KIEL END--------------------------------"
	fi
#BARCODE SPLITTER REVERSE READ SEARCH
else
	echo -e "------------------------OWN BARCODE SPLITTER REVERSE READ SEARCH BEGIN--------------------------------"
	#check if forward read is compressed or not
	#if [[ ${unpackedfastq: -3} != ".gz" ]]
	#then 
		echo "java -jar ${PATHOwnBarcodeSplitterReverseReadSearch} ${unpackedfastq} ${packedfastqrev} ${barcode} ${pwd} ${OwnBarcodeSplitterReverseReadSearchParameter}"
		java -jar $PATHOwnBarcodeSplitterReverseReadSearch $unpackedfastq $packedfastqrev $barcode ${pwd} ${OwnBarcodeSplitterReverseReadSearchParameter}

#TODO fix broken pipe error https://blog.nelhage.com/2010/02/a-very-subtle-bug/
#else	
#		echo "java -jar $PATHOwnBarcodeSplitterReverseReadSearch <(gunzip -c ${unpackedfastq}) <(gunzip -c ${packedfastqrev}) ${barcode} ${pwd} ${OwnBarcodeSplitterReverseReadSearchParameter}"
#		java -jar $PATHOwnBarcodeSplitterReverseReadSearch <(gunzip -c $unpackedfastq) <(gunzip -c $packedfastqrev) $barcode ${pwd} ${OwnBarcodeSplitterReverseReadSearchParameter}
#fi

	for i in "${!filenamesfilearr[@]}";do
		mkdir ${outputpath}/${filenamesfilearr[i]};
		mv ${barcodearr[i]}.fastq ${outputpath}/${filenamesfilearr[i]}/${filenamesfilearr[i]}.fastq;
		mv ${barcodearr[i]}_rev.fastq ${outputpath}/${filenamesfilearr[i]}/${filenamesfilearr[i]}_rev.fastq;
	done

	if [ $ERRORCHECK == 1 ]
	then
		#check if reserve string differs only in one pos for first 5 'headlines'
		for var in "${filenamesfilearr[@]}"; do
			for i in {1..5}
			do
				#cut the every 4th line out of the file
				r1=$(sed -n "$((4*i+1))p" ${outputpath}/$var/$var.fastq)
				r2=$(sed -n "$((4*i+1))p" ${outputpath}/$var/${var}_rev.fastq)
				#check if lines differs in only one pos
				diffcount=0
				for (( i=0; i<${#r1}; i++ )); do
					c1=${r1:$i:1}
					c2=${r2:$i:1}
				  if [ "$c1" != "$c2" ]
				  then
					if [ "$c1" == "1" ] && [ "$c2" == "2" ]
					then
						((diffcount++))
					fi
				  fi
				done
				#if string differs more or less, let the user decide what to do
				if (( $diffcount != 1 ))
				then
					echo "an error was found in ${var}!"
					echo $r1
					echo $r2
					read -p "continue? " ok
				#else
					#echo "${var}_rev.fastq seems ok"
				fi
			done
		done
	fi
		
	echo -e "------------------------OWN BARCODE SPLITTER REVERSE READ SEARCH END--------------------------------"
fi

#wait...
if [ $SILENT != 1 ]
	then
		read -p "continue with pear?" ok
fi

#get number of CPU cores
if [ "$(uname)" == "Darwin" ]; then
    cores=$(sysctl -n hw.ncpu)
else
    cores=$(grep -c ^processor /proc/cpuinfo)
fi

if [ $USEPEAR == 1 ]
then
	echo -e "------------------------PEAR BEGIN------------------------------------"
	for var in "${filenamesfilearr[@]}"; do

		echo "$PATHpear -f ${outputpath}/$var/$var.fastq -r ${outputpath}/$var/${var}_rev.fastq \
		-o ${outputpath}/$var/${var} -j $cores $PARApear"

		(time $PATHpear -f ${outputpath}/$var/$var.fastq -r ${outputpath}/$var/${var}_rev.fastq \
		-o ${outputpath}/$var/${var} -j $cores $PARApear >> ${assemblerlogfilepath}/${var}.txt) 2> timePearTmp.txt
		cat timePearTmp.txt >> ${assemblerlogfilepath}/${var}.txt
		while read line; do
			if [[ $line == *"real"* ]] || [[ $line == *"user"* ]] || [[ $line == *"sys"* ]] || [[ -z "$line" ]]
			then
				continue
			fi
			echo $line
		done < timePearTmp.txt
		#cat timePearTmp.txt
	
	done
	rm timePearTmp.txt
	echo -e "------------------------PEAR END--------------------------------------"

	#ask for remove unassembled files
	answer=""
	if [ $SILENT != 1 ]
		then
			read -p "remove files with unassmbled reads (type 'yes' or 'no')?" answer
		else
			answer="yes"
	fi

	if [ $answer = "yes" ]
	then 
		for var in "${filenamesfilearr[@]}"; do
			rm ${outputpath}/$var/$var.unassembled.forward.fastq
			rm ${outputpath}/$var/$var.unassembled.reverse.fastq
			rm ${outputpath}/$var/$var.discarded.fastq
		done
	fi
fi
#FLASH
if [ $FLASH == 1 ]
then
	echo -e "------------------------FLASH BEGIN------------------------------------"
	for var in "${filenamesfilearr[@]}"; do

		echo "$PATHflash ${outputpath}/$var/$var.fastq ${outputpath}/$var/${var}_rev.fastq \
		-t 2 --to-stdout -M 9999 > ${outputpath}/$var/${var}.assembled.fastq"

		$PATHflash ${outputpath}/$var/$var.fastq ${outputpath}/$var/${var}_rev.fastq \
		-t 2 --to-stdout -M 9999 > ${outputpath}/$var/${var}.assembled.fastq 2> timeFlashTmp.txt
		cat timeFlashTmp.txt >> ${assemblerlogfilepath}/${var}.txt
	done
	echo -e "------------------------FLASH END--------------------------------------"
fi

#wait
if [ $SILENT != 1 ]
	then
		read -p "continue with mitcr?" ok
fi

if [ $USEMITCR == 1 ]
then
	echo -e "------------------------MITCR BEGIN--------------------------------"

	mkdir ${outputpath}/cls

	#first do the processing part
	for var in "${filenamesfilearr[@]}"; do
		echo "java -Xmx2g -jar $PATHmitcr $PARAmitcr ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/cls/${var}.cls"
		java -Xmx2g -jar $PATHmitcr $PARAmitcr ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/cls/${var}.cls
	done

	#mkdir ${outputpath}/xls

	#first do the processing part
	#for var in "${filenamesfilearr[@]}"; do
	#	echo "java -Xmx2g -jar $PATHmitcr $PARAmitcr ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/xls/${var}.xls"
	#	java -Xmx2g -jar $PATHmitcr $PARAmitcr ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/xls/${var}.xls
	#done

	#mkdir ${outputpath}/tcr

	#second do the processing part for tcr
	for var in "${filenamesfilearr[@]}"; do
		echo "java -Xmx2g -jar $PATHmitcr $PARAmitcrTCRPACKAGE ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/${outputfoldertables}/${var}.xls"
		java -Xmx2g -jar $PATHmitcr $PARAmitcrTCRPACKAGE ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/${outputfoldertables}/${var}.xls
	done
	echo -e "------------------------MITCR END--------------------------------"
fi

if [ $USEMIXCR == 1 ]
then
	echo -e "------------------------MIXCR BEGIN--------------------------------"
	#do the mixcr part

	
	#mkdir ${outputpath}/mixcr
	#java -Xmx4g -Xms3g -jar mixcr.jar align --loci TRB -s mmu -f --report alignmentReport.log Blut_C_15388_10_i11_i11_TACGTA.assembled.fastq  alignments.vdjca
	#java -jar mixcr.jar assemble --report assembleReport.log alignments.vdjca clones.clns
	#java -jar mixcr.jar exportClones clones.clns clones.txt
	for var in "${filenamesfilearr[@]}"; do
		#algin
		if [ $USEMIXCRASSEMBLING == 1 ]
		then
			echo "java -Xmx2g -jar $PATHmixcr align $PARAmixcrAlign --report ${outputpath}/$var/${var}_align.log ${outputpath}/$var/$var.fastq ${outputpath}/$var/${var}_rev.fastq ${outputpath}/${outputfoldertables}/${var}.vdjca"
			java -Xmx2g -jar $PATHmixcr align $PARAmixcrAlign --report ${outputpath}/$var/${var}.log ${outputpath}/$var/$var.fastq ${outputpath}/$var/${var}_rev.fastq ${outputpath}/${outputfoldertables}/${var}.vdjca
		else
			echo "java -Xmx2g -jar $PATHmixcr align $PARAmixcrAlign --report ${outputpath}/$var/${var}_align.log ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/${outputfoldertables}/${var}.vdjca"
			java -Xmx2g -jar $PATHmixcr align $PARAmixcrAlign --report ${outputpath}/$var/${var}.log ${outputpath}/$var/${var}.assembled.fastq ${outputpath}/${outputfoldertables}/${var}.vdjca
		fi
		#assemble
		echo "java -Xmx2g -jar $PATHmixcr assemble $PARAmixcrAssemble --report ${assemblerlogfilepath}/${var}.txt ${outputpath}/${outputfoldertables}/${var}.vdjca ${outputpath}/$var/${var}.clns"
		java -Xmx2g -jar $PATHmixcr assemble $PARAmixcrAssemble --report ${assemblerlogfilepath}/${var}.txt ${outputpath}/${outputfoldertables}/${var}.vdjca ${outputpath}/$var/${var}.clns
		#exportClones
		echo "java -Xmx2g -jar $PATHmixcr exportClones $PARAmixcrExportClones ${outputpath}/$var/${var}.clns ${outputpath}/${outputfoldertables}/${var}.xls"
		java -Xmx2g -jar $PATHmixcr exportClones $PARAmixcrExportClones ${outputpath}/$var/${var}.clns ${outputpath}/${outputfoldertables}/${var}.xls
	done
	echo -e "------------------------MIXCR END--------------------------------"
fi
#-----------------DATA PROCESSING END-------------------------------------

#-----------------BEGIN EVALUTION-----------------------------------------
#wait
if [ $SILENT != 1 ]
	then
		read -p "continue with evalution?" ok
fi

assembler_sequence_List=()
assembler_assembled_List=()
assembler_overall_List=()
assembler_percent_List=()

#ID & Cryo-Nr. & seq  & Raw & nach PEAR     & mitcr & mitcr ohne 1er & clonotypes 
#15388_14, 1800 t, i9 , ATGATA & 3.1 & 3.05 & 90904 & 75604 & 65306

#get assembler log files

for var in "${filenamesfilearr[@]}"; do
	#always create in entry in the lists 
	assembled="assembler error"
	overall="assembler error"
	percent="assembler error"

	if [ $USEPEAR == 1 ]
	then
		while read line; do
			#Assembled reads ...................: 3,666 / 4,294 (85.375%)
			# f1	    f2		f3					f4  f5	f6		f7
			if [[ $line == *"Assembled reads"* ]] && [[ $line != *"file"* ]]
			then
				assembled=$(echo $line | cut -d ' ' -f4 | tr -d ,) 
				overall=$(echo $line | cut -d ' ' -f6 | tr -d ,) 
				#remove first and last char '(85.375%)' to '85.375%'
				percent=$(echo $line | cut -d ' ' -f7 | sed -e 's/^(//' -e 's/%)$//') 
				#echo "$percent" 
				break #because there is line with 'Assembled reads file' left in the log file
			fi
		done < ${assemblerlogfilepath}/${var}.txt
	fi

	if [ $FLASH == 1 ]
	then
		while read line; do
			#[FLASH]     Total pairs:      5860
			if [[ $line == *"Total pairs:"* ]]
			then
				overall=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015') 
			fi
			#[FLASH]     Combined pairs:   1022
			if [[ $line == *"Combined pairs:"* ]]
			then
				assembled=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015') 
			fi
			#[FLASH]     Percent combined: 17.44%
			if [[ $line == *"Percent combined:"* ]]
			then
				percent=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015' | sed 's/.$//') 
			fi
		done < ${assemblerlogfilepath}/${var}.txt
	fi

	if [ $USEMIXCRASSEMBLING == 1 ]
	then
		while read line; do
			#[FLASH]     Total pairs:      5860
			if [[ $line == *"Final clonotype count:"* ]]
			then
				overall=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015') 
			fi
			#[FLASH]     Combined pairs:   1022
			if [[ $line == *"Total reads used in clonotypes:"* ]]
			then
				assembled=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015') 
			fi
			#[FLASH]     Percent combined: 17.44%
			if [[ $line == *"Reads used, percent of total:"* ]]
			then
				percent=$(echo $line | cut -d ':' -f2 | tr -d '\040\011\012\015' | sed 's/.$//') 
			fi
		done < ${assemblerlogfilepath}/${var}.txt
	fi
	
	assembler_sequence_List+=( "${var}" )
	assembler_assembled_List+=( "${assembled}" )
	assembler_overall_List+=( "${overall}" )
	assembler_percent_List+=( "${percent}" )
done


readonly assembler_sequence_List
readonly assembler_assembled_List
readonly assembler_overall_List
readonly assembler_percent_List

#Cryo.Nr & ID & time & RNA & raw & after PEAR(\%) & mitcr(\%) & 
#ct mitcr & wo 1er(\%) & ct wo 1er(\%) & wo oof and stops & 
#ct wo oof and stops(\%) & $\Delta$ ct \\ \hline \hline

echo "Cryo.Nr & ID & time & RNA & Raw & post assembling(\%)" >> ${texlogfile}
echo "Cryo.Nr & ID & time & RNA & Raw & post assembling(\%)" >> ${texlogfilehighprecision}
for i in "${!assembler_assembled_List[@]}";do
	#echo "${assembler_assembled_List[i]} ${assembler_overall_List[i]} ${assembler_percent_List[i]}"
	assembled_r=$(echo "${assembler_assembled_List[i]}")
	overall_r=$(echo "${assembler_overall_List[i]}")
	assembled_r_percent=`perl -MPOSIX -e "printf(\"%.0f\", ${assembled_r}/${overall_r}*100);"`
	assembled_r_percent_high_precision=`perl -MPOSIX -e "printf(\"%.3f\", ${assembled_r}/${overall_r}*100);"`
	#assembled_r_percent=`perl -MPOSIX -e "printf(\"%.0f\", ${assembler_percent_List[i]});"`
	#if more than 100000 assembled seqs exists, than multiply with 10^-6  
	if (( $assembled_r > 100000 ))
	then
		assembled_r=`perl -MPOSIX -e "printf(\"%.2f\", ${assembled_r}/1000000);"`
	else
		assembled_r=`perl -MPOSIX -e "printf(\"%.0f_low\", ${assembled_r});"`
	fi
	if (( $overall_r > 100000 ))
	then
		overall_r=`perl -MPOSIX -e "printf(\"%.2f\", ${overall_r}/1000000);"`
	else
		overall_r=`perl -MPOSIX -e "printf(\"%.0f_low\", ${overall_r});"`
	fi
	#restore the escape chars in the Names
	cyroNrTmp=$(echo "${cryoNrarr[i]}" | sed -E "s/${subsDot}+/./g" | sed -E "s/${subsBackslash}+/\\\/g" \
	|  sed -E "s/[_]+/${subsUnderscore}/g")
	IDarrTmp=$(echo "${IDarr[i]}" | sed -E "s/${subsDot}+/./g" | sed -E "s/${subsBackslash}+/\\\/g" \
	|  sed -E "s/[_]+/${subsUnderscore}/g")	

	#define the output format here!
	#Cryo.Nr & ID & Zeitpunkt & RNA & Raw & nach PEAR & mitcr & mitcr ohne 1er & clonotypes &\Delta ct \\ \hline 
	echo "${cyroNrTmp} & ${IDarrTmp} & ?d & ?ng & ${overall_r} & ${assembled_r}(${assembled_r_percent})" >> ${texlogfile}
	echo "${cyroNrTmp} & ${IDarrTmp} & ?d & ?ng & ${assembler_overall_List[i]} & ${assembler_assembled_List[i]}\
(${assembled_r_percent_high_precision})" >> ${texlogfilehighprecision}
done
#-----------------EVALUTION END-------------------------------------
echo "Execution finished"
