package Listener;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;
import app.HelpMethods;

public class IdListener implements DocumentListener {

	private List<String> listStrIdCache = new ArrayList<String>();

	@Override
	public void changedUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	public void autocompleteIdOrBarcode(DocumentEvent e) {
		listStrIdCache.clear();
		// get the owner of this document
		// JTextField owner = (JTextField) e.getDocument().getProperty("owner");
		// get the neighbor of this object i.e. id or barcode field
		// JTextField neighbor = (JTextField) e.getDocument().getProperty(
		// "neighbor");

		// determine if it a Id or Barcode Field
		String type = (String) e.getDocument().getProperty("type");

		GlobalVars gv = GlobalVars.singleton();

		if (type.equals("id")) {

			for (int i = 0; i < gv.getNumberOfSamples(); i++) {
				JTextField owner = gv.getTxtSamplesId(i);
				JTextField neighbor = (JTextField) owner.getDocument()
						.getProperty("neighbor");

				if (owner.getText().equals(""))
					owner.setBackground(GlobalVars.colorDefault);
				else {
					String id = HelpMethods.parseId(owner.getText());
					if (id != null) {
						// setText only if the barcode isn't correct
						String barcode = HelpMethods.getBarcodeFromId(id);
						if (gv.getAutoComplete()
								&& !barcode.equals(neighbor.getText())) {
							neighbor.setText(barcode);
						}
						owner.setBackground(GlobalVars.colorValid);
					} else
						owner.setBackground(GlobalVars.colorInvalid);
				}
			}
			gv.detectDupsInTxtSampleId();

		}

	}
}
