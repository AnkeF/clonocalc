package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import app.GlobalVars;
import app.HelpMethods;

public class StartButtonListener implements ActionListener {

	public static final String confName = GlobalVars.singleton().confFileName;
	public static final String confAbsoluteFilePath = GlobalVars.singleton().userDir + File.separator + confName;
	public static final String confNameBackup = GlobalVars.singleton().confBackupFileName;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		File file = new File(confAbsoluteFilePath);
		GlobalVars gv = GlobalVars.singleton();

		// if exists make a backup
		if (file.exists()) {
			File backupFile = new File(confNameBackup);
			if (backupFile.exists())
				backupFile.delete();
			file.renameTo(new File(confNameBackup));
		}

		// write new config to file, overwrites the old one
		PrintWriter writer;
		try {
			writer = new PrintWriter(confAbsoluteFilePath, "UTF-8");

			// create the name,id,barcode cache
			List<String> listStrNameCache = new ArrayList<String>();
			List<String> listStrIdCache = new ArrayList<String>();
			List<String> listStrBarcodeCache = new ArrayList<String>();

			// Write file names to conf
			// File forwardRead = new File(gv.txtForwardReadFile.getText());
			// File reverseRead = new File(gv.txtReverseReadFile.getText());
			// System.out.println(forwardRead.getAbsolutePath());
			// System.out.println(forwardRead.getParent());

			writer.println(gv.confVOutputPath + " = " + gv.txtOutputFolder.getText());
			writer.println(gv.confVForwardReadFile + " = " + gv.txtForwardReadFile.getText());
			writer.println(gv.confVReverseReadFile + " = " + gv.txtReverseReadFile.getText());
			writer.println("\n");

			// write parameters to conf
			if (GlobalVars.internalVersion) {
				writer.println(gv.confVPARAbarcodesplitter + " = \"" + gv.txtParaBarcodeSplitter.getText() + "\"");
				writer.println(gv.confVPARAmitcr + " = \"" + gv.txtParaMitcr.getText() + "\"");
				writer.println(gv.confVPARAmitcrTCRPACKAGE + " = \"" + gv.txtParaMitcrTcrPackage.getText() + "\"");
				writer.println(gv.confVPARApear + " = \"" + gv.txtParaPear.getText() + "\"");
			}

			writer.println(gv.confVPARAmixcrAlign + " = \"" + gv.txtParaMixcrAlign.getText() + "\"");
			writer.println(gv.confVPARAmixcrAssemble + " = \"" + gv.txtParaMixcrAssemble.getText() + "\"");
			writer.println(gv.confVPARAmixcrExportClones + " = \"" + gv.txtParaMixcrExportClones.getText() + "\"");
			writer.println(gv.confVBarcodeFile + " = " + gv.txtBarcodeFile.getText());
			writer.println("\n");

			// Write samples to conf
			// T_1800 & C_15388.14 & ATGATA
			for (int i = 0; i < gv.getNumberOfSamples(); i++) {
				// proof validation for each line
				String line = "";
				if (HelpMethods.isValidName(listStrNameCache, gv.getTxtSamplesName(i).getText()))
					line += gv.getTxtSamplesName(i).getText() + " & ";
				else
					continue;

				String strId = gv.getTxtSamplesId(i).getText();
				String id = HelpMethods.parseId(strId);
				if (!strId.contains("i"))
					line += "i";
				if (HelpMethods.isValidName(listStrIdCache, strId) && id != null)
					line += strId + " & ";
				else
					continue;

				String strBarcode = gv.getTxtSamplesBarcode(i).getText();
				if (HelpMethods.isValidName(listStrBarcodeCache, strBarcode) && HelpMethods.isValidBarcode(strBarcode))
					line += strBarcode;
				else
					continue;

				writer.println(line);
			}
			writer.close();

			// copy the config file to forward read folder
			File source = new File(confAbsoluteFilePath);
			File dest = HelpMethods.getFile(new File(gv.txtForwardReadFile.getText()), confName);
			try {
				copyFileUsingStream(source, dest);
				// don't work on mac os
				// Files.copy(source.toPath(), dest.toPath(), REPLACE_EXISTING);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// start execution
		gv.bashExecution.executeScript();

	}

	private static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

}
