package app;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class BashExecution {

	private Process proc = null;

	public void executeScript() {
		GlobalVars gv = GlobalVars.singleton();
		// if proc is running
		if (proc != null && isRunning(proc))
			return;

		try {
			// proc = Runtime.getRuntime().exec("bash -u test.sh");
			ProcessBuilder builder;
			List<String> commands = new ArrayList<String>();
			commands.add("bash");
			commands.add("-u");
			commands.add(gv.userDir + File.separator + "meins.sh");
			commands.add("-silent");
			commands.add("-cd " + gv.userDir);
			if (gv.getUseOwnMultiplexer())
				commands.add("-owndemultiplexer");
			if (GlobalVars.internalVersionUseMixcr)
				commands.add("-mixcr");
			if (GlobalVars.internalVersion) {
				commands.add("-pear");
				commands.add("-mitcr");
			}
			if (!GlobalVars.internalVersion) {
				// deprecated use mixcr instead of flash
				// commands.add("-flash");
				commands.add("-mixcr");
				commands.add("-mixcrAssembling");
				commands.add("-maxHammingDist" + gv.getMaxHammingDist());
				commands.add("-barcodeStartPos" + gv.getBarcodeStartPos());
			}
			if (!OSDetection.isMac())
				commands.add("-linux");
			if (!gv.errorChecking)
				commands.add("-noerrorcheck");

			builder = new ProcessBuilder(commands);
			builder.redirectErrorStream(true);
			proc = builder.start();

			final Process runningProc = proc;
			// final JTextArea textAreaNewThread =
			// GlobalVars.singleton().textArea;
			final JTextPane txtpNewThread = GlobalVars.singleton().txtpDebug;
			final ActionListener pipelineFinishedListener = GlobalVars
					.singleton().PipelineFinshedListener;

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						// measure runtime
						TimeLogger timelog = new TimeLogger();

						InputStream stdout = runningProc.getInputStream();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(stdout));

						String line = null, lineold = "Start execution";
						while ((line = reader.readLine()) != null) {
							// System.out.println("Main: " + line);
							append(txtpNewThread, lineold + "\n", null);
							timelog.measureTime(lineold);
							lineold = line;
						}

						if (lineold.contains("Execution finished")) {
							// print measured time to text pane
							timelog.measureTime(lineold);
							// main execution part finished
							append(txtpNewThread, "Tables generated\n",
									Color.BLUE);
							append(txtpNewThread, "Proceed with R\n",
									Color.BLUE);
							reader.close();
							stdout.close();

							// start the R part
							// check for R-file
							if (!(new File(GlobalVars.singleton().userDir
									+ File.separator + "R/autoR.sh")).exists())
								append(txtpNewThread, "autoR.sh in folder "
										+ GlobalVars.singleton().userDir
										+ File.separator + "R is missing!",
										Color.RED);

							List<String> commands = new ArrayList<String>();
							commands.add("bash");
							commands.add("-u");
							commands.add(GlobalVars.singleton().userDir
									+ File.separator + "R/autoR.sh");
							commands.add("-cd "
									+ GlobalVars.singleton().userDir);
							ProcessBuilder builder = new ProcessBuilder(
									commands);
							builder.redirectErrorStream(true);
							proc = builder.start();

							stdout = proc.getInputStream();
							reader = new BufferedReader(new InputStreamReader(
									stdout));

							line = null;
							lineold = "Executre R script";

							while ((line = reader.readLine()) != null) {
								// System.out.println("Rscript: " + line);
								append(txtpNewThread, lineold + "\n", null);
								lineold = line;
							}

							if (lineold.contains("R script finished")) {
								timelog.measureTime(lineold);
								append(txtpNewThread, lineold + "\n",
										Color.BLUE);
								append(txtpNewThread, timelog.getReport(),
										Color.BLUE);
								append(txtpNewThread,
										"Execution finished successfully",
										Color.BLUE);
								pipelineFinishedListener.actionPerformed(null);
							} else {
								append(txtpNewThread, "Some error occured",
										Color.RED);
								// System.out
								// .println("Rscript: No finished signal.");
							}

						} else {
							// System.out.println("Main: No finished signal.");
						}

						// System.out.println("Bash execution thread
						// finished.");
						reader.close();
						stdout.close();
					} // signal main thread
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void stopScript() {
		// if there any process to stop?
		if (proc == null)
			return;

		// System.out.println("try to stop script");

		// kill running script
		Runtime r = Runtime.getRuntime();
		try {
			r.exec(new String[] { "bash", "-c",
					"pkill -9 -f 'bash -u meins.sh'" });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean isRunning(Process process) {
		try {
			process.exitValue();
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	public void append(final JTextPane txtp, final String str, final Color color) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Document doc = txtp.getDocument();
				StyledDocument doc = txtp.getStyledDocument();
				Style style = null;
				if (color != null) {
					style = txtp.addStyle("I'm a Style", null);
					StyleConstants.setForeground(style, color);
					StyleConstants.setFontSize(style, 17);
				}

				if (doc != null) {
					try {
						// doc.insertString(doc.getLength(), str, null);
						doc.insertString(doc.getLength(), str, style);
					} catch (BadLocationException e) {
					}
				}
			}
		});

	}
}