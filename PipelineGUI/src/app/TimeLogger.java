package app;

import java.util.concurrent.TimeUnit;

public class TimeLogger {
	private long startTime = 0;
	private long afterBCSTime = 0;
	private long afterKielTime = 0;
	private long afterPearTime = 0;
	private long afterMitcrTime = 0;
	private long afterLogFirstTime = 0;
	private long afterRscriptTime = 0;
	private long afterOwnDemultiplexer = 0;

	public TimeLogger() {
		startTime = System.nanoTime();
	}

	public String getTimeInMandS(long startTime, long endTime) {
		long elapsedTime = endTime - startTime;
		long elapsedSeconds = TimeUnit.SECONDS.convert(elapsedTime,
				TimeUnit.NANOSECONDS);
		long seconds = elapsedSeconds % 60;
		long minutes = elapsedSeconds / 60;
		return minutes + "m " + seconds + "s";
	}

	public void measureTime(String line) {
		if (line.contains("--OUTPUT BARCODE SPLITTER END--")) {
			afterBCSTime = System.nanoTime();
		} else if (line.contains("--KIEL END--")) {
			afterKielTime = System.nanoTime();
		} else if (line.contains("--PEAR END--")) {
			afterPearTime = System.nanoTime();
		} else if (line.contains("--MITCR END--")) {
			afterMitcrTime = System.nanoTime();
		} else if (line.contains("Execution finished")) {
			afterLogFirstTime = System.nanoTime();
		} else if (line.contains("R script finished")) {
			afterRscriptTime = System.nanoTime();
		} else if (line.contains("OWN DEMULTIPLEXER END")) {
			afterOwnDemultiplexer = System.nanoTime();
		}
	}

	public String getReport() {
		if (afterRscriptTime == 0)
			return "";

		String report = "";
		if (startTime > 0 && afterBCSTime > 0)
			report = "BC split:	" + getTimeInMandS(startTime, afterBCSTime)
					+ "\n";
		if (afterKielTime > 0 && afterBCSTime > 0)
			report += "Kiel:	" + getTimeInMandS(afterBCSTime, afterKielTime)
					+ "\n";
		if (afterOwnDemultiplexer > 0 && startTime > 0)
			report += "Own Demultiplexer:	"
					+ getTimeInMandS(startTime, afterOwnDemultiplexer) + "\n";
		if (afterKielTime > 0 && afterPearTime > 0)
			report += "Pear:	" + getTimeInMandS(afterKielTime, afterPearTime)
					+ "\n";
		if (afterPearTime > 0 && afterMitcrTime > 0)
			report += "MiTCR:	" + getTimeInMandS(afterPearTime, afterMitcrTime)
					+ "\n";
		if (afterMitcrTime > 0 && afterLogFirstTime > 0)
			report += "Log:	"
					+ getTimeInMandS(afterMitcrTime, afterLogFirstTime) + "\n";
		if (afterLogFirstTime > 0 && afterRscriptTime > 0)
			report += "Rscript:	"
					+ getTimeInMandS(afterLogFirstTime, afterRscriptTime)
					+ "\n";
		if (startTime > 0 && afterRscriptTime > 0)
			report += "Summary: " + getTimeInMandS(startTime, afterRscriptTime)
					+ "\n";

		return report;
	}
}
