package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JTextField;

public class HelpMethods {
	// static class
	private HelpMethods() {
	}

	private static HashMap<String, String> idBarcodeMap = new HashMap<String, String>();;
	private static HashMap<String, String> barcodeIdMap = new HashMap<String, String>();;

	public static final void parseBarcodes(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			for (String line; (line = br.readLine()) != null;) {
				// skip for whitespace and comment lines
				if (line.trim().length() == 0)
					continue;
				if (line.trim().charAt(0) == '#')
					continue;

				String[] splittedLine = line.split("=");
				if (splittedLine.length != 2) {
					System.out.println("\"" + line
							+ "\" is not separable by '='");
					continue;
				}

				String id = splittedLine[0].trim();
				String barcode = splittedLine[1].trim();

				idBarcodeMap.put(id, barcode);
				barcodeIdMap.put(barcode, id);
			}
		} catch (FileNotFoundException e) {
			System.out.println(file.getAbsolutePath() + " not found!");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static final String parseId(String text) {
		if (idBarcodeMap.containsKey(text))
			return text;
		return null;
	}

	public static final boolean isValidBarcode(String text) {
		return text.matches("[A|C|G|T]{6}");
	}

	public static final String getBarcodeFromId(String id) {
		return idBarcodeMap.get(id);
		// return illumina_barcodes[id - 1];
	}

	public static final String getIdFromBarcode(String barcode) {
		String object;
		if ((object = barcodeIdMap.get(barcode)) != null)
			return object;
		else
			return null;
	}

	// calc the absolute path to a given filename from another file in the same
	// dir
	public static final File getFile(File fileDestFolder, String strFileName) {
		if (fileDestFolder == null)
			return null;
		String pathToGuess = fileDestFolder.getAbsolutePath().substring(
				0,
				fileDestFolder.getAbsolutePath().indexOf(
						fileDestFolder.getName()));
		return new File(pathToGuess + strFileName);
	}

	// proof if id is not empty and used only once
	public static boolean isValidName(List<String> listStrCache, String name) {
		if (name.equals(""))
			return false;
		// is id only used once?
		for (String cachedName : listStrCache)
			if (name.equals(cachedName))
				return false;

		// add id to "used"
		listStrCache.add(name);
		return true;
	}

	public static void detectDuplicateInJTextFieldList(List<JTextField> listTxt) {
		List<String> listStrCache = new ArrayList<String>();
		for (JTextField txt : listTxt) {
			if (txt.getText().equals(""))
				txt.setBackground(GlobalVars.colorDefault);
			else if (!HelpMethods.isValidName(listStrCache, txt.getText()))
				txt.setBackground(GlobalVars.colorInvalid);
			// don't define a JTextField as valid, because maybe there are other
			// validation checks
		}
	}
}

// i1 = GTGTCA
// i2 = TCTGAC
// i3 = TGACTA
// i4 = CGCTCT
// i5 = AGATGA
// i6 = ACTCAT
// i7 = TCGCTC
// i8 = AGACAC
// i9 = ATGATA
// i10 = CGTGAT
// i11 = TACGTA
// i12 = TGAGCG
// i13 = TACAGT
// i14 = TGCACA
// i15 = CATGTC
// i16 = CTGACT
// i17 = TGTATC
// i18 = AGCAGC
// i19 = TCACGT
// i20 = GACGAT