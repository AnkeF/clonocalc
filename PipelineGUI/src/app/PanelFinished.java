package app;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import net.miginfocom.swing.MigLayout;

public class PanelFinished implements ActionListener {
	MainFrame mainFrame = null;
	JFrame outerFrame = null;

	JPanel panelFinished = new JPanel();
	JPanel panelButtons = new JPanel();

	JTextPane txtpDebug = GlobalVars.singleton().txtpDebug;
	JScrollPane spTxtpDebug = GlobalVars.singleton().spTxtpDebug;

	JButton btnShowLogFile = new JButton("Show log folder");
	JButton btnShowTables = new JButton("Show table folder");
	JButton btnShowResultFolder = new JButton("Show result folder");
	JButton btnBackToStart = new JButton("Back to start");

	public JPanel getPanel() {
		return panelFinished;
	}

	public PanelFinished(MainFrame mainFrame, JFrame outerFrame) {
		this.mainFrame = mainFrame;
		this.outerFrame = outerFrame;

		initComponents();
		// buildFinishedScreen();
	}

	private void initComponents() {
		btnShowLogFile.addActionListener(this);
		btnShowTables.addActionListener(this);
		btnShowResultFolder.addActionListener(this);
		btnBackToStart.addActionListener(this);
	}

	private void buildFinishedScreen() {
		panelFinished.setLayout(new BorderLayout());
		panelButtons.setLayout(new MigLayout());

		// spTxtpDebug.setSize(300, 300);
		panelFinished.add(spTxtpDebug, BorderLayout.CENTER);

		panelButtons.add(btnShowLogFile);
		panelButtons.add(btnShowTables);
		panelButtons.add(btnShowResultFolder);
		panelButtons.add(btnBackToStart);

		panelFinished.add(panelButtons, BorderLayout.SOUTH);
	}

	public void repaintFinishedScreen() {
		// first remove all components from the panel
		panelFinished.removeAll();
		// add the components to the panel
		buildFinishedScreen();

		// outerFrame.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BufferedReader br = null;
		String outputFolderName = "";
		try {

			br = new BufferedReader(new FileReader(
					GlobalVars.singleton().userDir + File.separator
							+ "lastTime.txt"));
			if ((outputFolderName = br.readLine()) == null) {
				throw new Exception(
						"Error in lastTime.txt. File seems to be empty.");
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		GlobalVars gv = GlobalVars.singleton();

		if (e.getSource() == this.btnShowLogFile) {
			File path = HelpMethods
					.getFile(new File(gv.txtForwardReadFile.getText()),
							outputFolderName);
			OpenFolder(path.toString() + "/log");
		} else if (e.getSource() == this.btnShowTables) {
			File path = HelpMethods
					.getFile(new File(gv.txtForwardReadFile.getText()),
							outputFolderName);
			OpenFolder(path.toString() + "/tables");
		} else if (e.getSource() == this.btnShowResultFolder) {
			File path = HelpMethods
					.getFile(new File(gv.txtForwardReadFile.getText()),
							outputFolderName);
			OpenFolder(path.toString());
		} else if (e.getSource() == this.btnBackToStart) {
			mainFrame.switchToConfigScreen();
		}

	}

	private void OpenFolder(String pathToFolder) {
		ProcessBuilder builder;
		if (GlobalVars.singleton().isLinuxOS)
			builder = new ProcessBuilder("xdg-open", pathToFolder);
		else
			builder = new ProcessBuilder("open", pathToFolder);

		try {
			Process proc = builder.start();
			proc.waitFor();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
